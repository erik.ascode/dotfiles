#!/usr/bin/env bash
cd ~/Downloads
curl -L -O "https://download01.logi.com/web/ftp/pub/techsupport/options/options_installer.zip"
unzip option_installer.zip
curl -L -O "https://github.com/JohnCoates/Aerial/releases/download/v2.3.3/Aerial.saver.zip"
unzip Aerial.saver.zip
curl -O "https://github.com/iotaledger/trinity-wallet/releases/download/desktop-1.6.2/trinity-desktop-1.6.2.dmg"
curl -L -O "https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-361.0.0-darwin-x86_64.tar.gz"
tar zxf google-cloud-sdk-361.0.0-darwin-x86_64.tar.gz
./google-cloud-sdk/install.sh -q --additional-components kubectl kubectl-oidc beta
curl -L -O "https://developers.yubico.com/yubikey-manager-qt/Releases/yubikey-manager-qt-latest-mac.pkg"

for i in `ls *.dmg`; do hdiutil attach -plist -nobrowse -readonly -noidme $i; done
for i in `ls *.app`; do sudo mv -v $i /Applications; done

curl -L https://get.oh-my.fish | fish
omf install bobthefish
curl https://git.io/fisher --create-dirs -sLo ~/.config/fish/functions/fisher.fish
cp -R fishfile ~/.config/fish/fishfile
fisher install <~/.config/fish/fishfile

sudo sed -i '' '1i\/usr/local/bin/fish' /etc/shells
chsh -s /usr/local/bin/fish

sudo easy_install pip3
sudo pip3 install ansible --quiet
pip3 install terraform-compliance --user
pip3 install c7n --user
pip3 install --user passlib
pip3 install boto3 --user

# Keybase
# echo -e 'Keybase\t/Volumes/Keybase' | sudo tee -a /etc/synthetic.conf
# /Library/Filesystems/kbfuse.fs/Contents/Resources/load_kbfuse

# for VS Code
sudo ln -s /usr/local/bin/ssh-askpass /usr/local/Cellar/openssh/8.4p1_1/libexec/ssh-askpass

# Needs login
# https://hub.docker.com/editions/community/docker-ce-desktop-mac