#!/usr/bin/env bash

# make sure ARM is used
export ARCHFLAGS='-arch arm64'

# Install command-line tools using Homebrew.
# first, let's install Brew
sudo xcode-select --install

/usr/bin/env bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# Save Homebrew’s installed location.
BREW_PREFIX=$(brew --prefix)

# Install recipes with Brewfile (need to update this with check for existence of Brewfile)
brew bundle

# Install GNU core utilities (those that come with macOS are outdated).
# Don’t forget to add `$(brew --prefix coreutils)/libexec/gnubin` to `$PATH`.
ln -s "${BREW_PREFIX}/bin/gsha256sum" "${BREW_PREFIX}/bin/sha256sum"

# Remove outdated versions from the cellar.
brew cleanup
